﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Unboxing
{
    public class NumPairsDivisibleBy60Class
    {
        [Required] public int[]? Time { get; set; }
    }
    public class CountPrimesClass
    {
        [Required] public int Number { get; set; }
    }
    public class MaxNumberOfBalloonsClass
    {
        [Required] public string? Text { get; set; }
    }
    public class LongestIdealStringClass
    {
        [Required] public string? StringValue { get; set; }
        [Required] public int IntValue { get; set; }
    }

    public class RemoveKdigitsClass
    {
        [Required] public string? StringValue { get; set; }
        [Required] public int Target { get; set; }
    }
}
