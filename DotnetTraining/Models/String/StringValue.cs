﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.ModelsString
{
    public class StringValue
    {
        [Required] public string Values { get; set; } = string.Empty;
    }
    public class StringArray
    {
        [Required] public string[]? Values { get; set; }
    }

    public class ReverseWordClass
    {
        [Required] public string? StringValues { get; set; }
    }

    public class FirstOccurrenceClass
    {
        [Required] public string? Values { get; set; }
        [Required] public string? Target { get; set; }
    }

    public class DecodeStringClass
    {
        public string? StringValues { get; set; }
    }
}
