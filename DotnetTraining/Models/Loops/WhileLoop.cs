﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Loops
{
    public class PermutationCLass
    {
        [Required] public int[]? Number { get; set; } 
    }

    public class SearchInsertPositionClass
    {
        [Required] public int[]? ArrayNumber { get; set; }
        [Required] public int Target { get; set; }
    }
    public class ContainsDuplicateClass
    {
        [Required] public int[]? DuplicteNumber { get; set; }
    }
    public class SingleNumberIIIClass
    {
        [Required] public int[]? SingleNumber { get; set; }
    }

    public class NumSquaresClass
    {
        [Required] public int Number { get; set; }
    }


    //-----------   This Program is used in Assignment Operator     ------------
    //public class NextPermutationClass
    //{
    //    public int[]? Number { get; set; }
    //}

}
