﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.ForLoops
{
    public class ForLoop1
    {
        [Required] public int[]? Values { get; set; }
        [Required] public int Target { get; set; }
    }


    public class DisappearedNumbersClass
    {
        [Required] public int[]? NumberArray { get; set; }
    }


    public class CombinationClass
    {
        [Required] public int Number { get; set; }
        [Required] public int Target { get; set; }
    }

    public class CountPrimeNumberClass
    {
        [Required] public int Number { get; set; }
    }

    public class ContainsDuplicateIIClass
    {
        [Required] public int[]? DuplicteNumber { get; set; }
        [Required] public int Target { get; set; }
    }

    public class LicenseKeyFormattingClass
    {
        [Required] public string? FormatString { get; set; }
        [Required] public int Target { get; set; }
    }
}
