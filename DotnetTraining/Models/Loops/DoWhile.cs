﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Loops
{
    public class PerfectNumberClass
    {
        [Required] public int Number { get; set; }
    }

    public class MajorityElementClass
    {
        [Required] public int[]? Number { get; set; }
    }
    public class NumberComplementClass
    {
        [Required] public int ComplementNumber { get; set; }
    }

    public class PascalsTriangleClass
    {
        [Required] public int NumberRows { get; set; }
    }

    public class CommonFactorsClass
    {
        [Required] public int Number1 { get; set; }
        [Required] public int Number2 { get; set; }
    }
}
