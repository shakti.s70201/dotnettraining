﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Loops
{
    public class RemoveDuplicatesClass
    {
        [Required] public string? Value { get; set; }
    }
    public class LargestNumberClass
    {
        [Required] public int[]? LargestNumber  { get; set; }
    }

    public class FindPeakElementClass
    {
        [Required] public int[]? Number { get; set; }
    }

    public class CoinChangeClass
    {
        [Required] public int[]? CoinNumber { get; set; }
        [Required] public int Amount { get; set; }
    }

    public class FindTheDifferenceClass
    {
        [Required] public string? Value1 { get; set; }
        [Required] public string? Value2 { get; set; }
    }
}
