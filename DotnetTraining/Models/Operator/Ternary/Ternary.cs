﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Ternary
{
    public class ClumsyFactorialClass
    {
        [Required] public int FactorialNumber { get; set; }
    }
    public class VersionNumberClass
    {
        public string? Version1 { get; set; }
        public string? Version2 { get; set; }
    }

    public class SubsetIIClass
    {
        [Required] public int[]? SubsetArray { get;set ; }
    }

    public class CountOperationsClass
    {
        [Required] public int Number1 { get; set; }  
        [Required] public int Number2 { get; set; }  
    }

    public class AddBinaryClass
    {
        public string? BinaryNumber1 { get; set; }
        public string? BinaryNumber2{ get; set; }
    }
}
