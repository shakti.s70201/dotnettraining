﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Comparision
{
    public class FourSumClass
    {
        [Required] public int[]? FourSumArray { get; set; }
        [Required] public int Target { get; set; }
    }

    public class AddStringsClass
    {
        [Required] public string? Number1 { get; set; }
        [Required] public string? Number2 { get; set; }
    }

    public class IsIsomorphicClass
    {
        [Required] public string? StringValue { get; set; }
        [Required] public string? Target { get; set; }
    }

    public class AddToArrayFormClass
    {
        [Required] public int[]? IntegerArray { get; set; }
        [Required] public int IntegerValue { get; set; }
    }
    public class DivideTwoIntegersClass
    {
        [Required] public int Dividend { get; set; }
        [Required] public int Divisor { get; set; }
    }

}
