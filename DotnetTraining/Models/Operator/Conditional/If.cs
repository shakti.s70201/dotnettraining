﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Conditional.If
{
    public class RangeMissingClass
    {
        [Required] public int[]? ArrayNumber { get; set; }
    }


    public class StringToIntegerClass
    {
        [Required] public string? StringValue { get; set; }
    }


    public class SquareRootClass
    {
        [Required] public int SqaureNumber { get; set; }
    }


    public class PowerClass
    {
        [Required] public double DoubleValue { get; set; }
        [Required] public int Number { get; set; }
    }



    public class SingleNumberClass
    {
        [Required] public int[]? SingleNumber {  get; set; }
    }
}
