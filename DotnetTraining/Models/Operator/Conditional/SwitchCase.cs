﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Conditional.SwitchCase
{
    public class SwitchCase1
    {
        [Required] public int[]? Values { get; set; }
        [Required] public int Target { get; set; }
        [Required] public string? OperatorSign { get; set; }
    }

    public class RelativeRanksClass
    {
        [Required] public int[]? Values { get; set; }
    }

    public class PermutationsIIClass
    {
        [Required] public int[]? PermutationsIIArray { get; set; }
    }

    public class RomanToIntegerClass
    {
        [Required] public string? RomanString { get; set; }
    }
}
