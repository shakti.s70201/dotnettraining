﻿using System.ComponentModel.DataAnnotations;
namespace DotnetTraining.Models.Operator.Conditional.IfElseLadder
{
    public class TruncatablePrimeCLass
    {
        [Required] public int Number { get; set; }
    }
   
    public class ValidParenthesesCLass 
    {
        [Required] public string? BracketsValue { get;set; }
    
    }
    public class FizzBuzzClass
    {
        [Required] public int Number { get; set; }
    }
    public class SearchInRotatedSortedArrayClass
    {
        [Required] public int[]? Number { get; set; }
        [Required] public int Target { get; set; }

    }
}
