﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Logical
{
    public class ThreeSumClass
    {
        [Required] public int[]? Numbers { get; set; }
    }
    public class ParenthesesClass
    {
        [Required] public string? BracketsValue { get; set; }
    }

    public class RotatedMirrorCLass
    {
        [Required] public int Number { get; set; }
    }

    public class StroboGrammaticClass
    {
        [Required] public string? StringNumber { get; set; }  
    }

    public class ReverseArrayPairsClass
    {
        [Required] public int[]? NumberArray { get; set; }
    }
}
