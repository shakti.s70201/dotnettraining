﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Bitwise
{
    public class Bitwise
    {
        [Required] public int[]? IntArrray { get; set; }
    }
    public class HammingDistanceClass
    {
        [Required] public int Number1 { get; set; }
        [Required] public int Number2 { get; set; }
    }

    public class NumberRangeClass
    {
        [Required] public int Left { get; set; }
        [Required] public int Right { get; set; }
    }
    public class FindingMissingNumberClass
    {
        [Required] public int[]? IntArrray { get; set; }
    }

    public class SingleNumberIIClass
    {
        [Required] public int[]? ArrayNumber { get; set; }
    }

}
