﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Arithmetic
{
    public class SumOfClosestTargetClass
    {
        [Required] public int[]? Numbers { get; set; }
        [Required] public int Target { get; set; }
    }
    public class AddSubMultiDiviClass
    {
        [Required] public int[]? Values { get; set; }
        [Required] public int Target { get; set; }
        [Required] public string? OperatorSign { get; set; }
    }

    public class FactorialClass
    {
        [Required] public int Number { get; set; }
    }

    public class MultiplyStringsClass
    {
        [Required] public string? Number1 { get; set; }
        [Required] public string? Number2 { get; set; }
    }
}
