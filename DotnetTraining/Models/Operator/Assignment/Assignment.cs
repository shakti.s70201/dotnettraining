﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Operator.Assignment
{
    public class NextPermutationClass
    {
        [Required] public int[]? Number { get; set; }
    }
    public class IntegerReplacementClass
    {
        [Required] public int Number { get; set; }
    }

    public class FibonacciNumberClass
    {
        [Required] public int FibonacciNumber { get; set; }
    }
    public class NumberOfStepsClass
    {
        [Required] public int Number { get; set; }
    }

    public class SimplifiedFractionsClass
    {
        [Required] public int Number { get; set; }
    }
}
