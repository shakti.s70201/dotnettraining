﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Array
{
    public class SumOfTwoArrayClass
    {
        [Required] public string[]? Array1 { get; set; }
        [Required] public string[]? Array2 { get; set; }
    }


    public class FindErrorNumsClass
    {
        [Required ]public int[]? Number{ get; set;}
    }

    public class PascalsTriangleIICLass
    {
        [Required] public int GetRows { get; set; }
    }

    public class PlusOneClass
    {
        [Required] public int[]? ArrayDigits { get; set; }
    }

    public class FourSumCountClass
    {
        [Required] public int[]? Number1 { get; set; }
        [Required] public int[]? Number2 { get; set; }
        [Required] public int[]? Number3 { get; set; }
        [Required] public int[]? Number4 { get; set; }
    }
}
