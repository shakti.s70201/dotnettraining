﻿using System.ComponentModel.DataAnnotations;

namespace DotnetTraining.Models.Array
{
    public class DiagonalTraverseClass
    {
        [Required] public int[][]? Matrix { get; set; }
    }

    public class MatrixDiagonalSumClass
    {
        [Required] public int[][]? MatrixSum { get; set; }
    }

    public class SortMatrixDiagonallyClass
    {
        [Required] public int[][]? MatrixSort { get; set; }
    }

    public class FloodFillClass
    {
        [Required] public int[][]? ImageArray { get; set; }
        [Required] public int Sr { get; set; }
        [Required] public int Sc { get; set; }
        [Required] public int Color { get; set; }
    }

    public class IslandPerimeterClass
    {
        [Required] public int[][]? IslandGrid { get; set; }
    }
}
