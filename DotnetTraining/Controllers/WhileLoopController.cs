﻿using DotnetTraining.Models.Loops;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DotnetTraining.Controllers
{
    [Route("api/loops/whileLoop/")]
    public class WhileLoopController : Controller
    {

        [HttpPost, Route("Permutation")]
        public IActionResult Permutation(PermutationCLass permutation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = permutation.Number;
                    List<IList<int>> result = new()
                    {
                        number!
                    };
                    int length = number!.Length;
                    int size, temp, i, j, k;
                    int[] temporaryArray;
                    IList<int> temporaryList;

                    i = 0;
                    while (i < length)
                    {
                        size = result.Count;
                        j = 0;
                        while (j < size)
                        {
                            temporaryArray = result[j].ToArray();
                            Array.Sort(temporaryArray, i, length - i);
                            result[j] = temporaryArray;
                            k = i + 1;
                            while (k < length)
                            {
                                temporaryList = new List<int>(result[j]);
                                if (temporaryList[k] == temporaryList[k - 1] ||
                                    temporaryList[k] == temporaryList[i])
                                {
                                    k++;
                                    continue;
                                }

                                temp = temporaryList[k];
                                temporaryList[k] = temporaryList[i];
                                temporaryList[i] = temp;
                                result.Add(temporaryList);
                                k++;
                            }
                            j++;
                        }
                        i++;
                    }

                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("searchInsertPosition")]
        public IActionResult SearchInsertPosition(SearchInsertPositionClass searchInsertPosition)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? arrayNumber = searchInsertPosition.ArrayNumber;
                    int target = searchInsertPosition.Target;
                    int start = 0, end = arrayNumber!.Length;

                    while (start < end)
                    {
                        var middle = start + (end - start) / 2;
                        if (arrayNumber[middle] == target) return Ok(middle);
                        else if (arrayNumber[middle] < target) start = middle + 1;
                        else end = middle;
                    }

                    return Ok(start);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("containsDuplicate")]
        public IActionResult ContainsDuplicate(ContainsDuplicateClass duplicte)
        {
            int[]? number = duplicte.DuplicteNumber;
            if (number!.Length == 1)
            {
                return Ok(false);
            }
            bool res = false;
            Array.Sort(number);
            int i = 1;
            while (i < number.Length)
            {
                if (number[i] == number[i - 1])
                {
                    res = true;
                    break;
                }
                i++;
            }
            return Ok(res);
        }




        [HttpPost,Route("singleNumberIII")]
        public IActionResult SingleNumberIII(SingleNumberIIIClass singleNumberIII)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = singleNumberIII.SingleNumber;
                    //int count = 0;
                    var integer = new int[2];
                    int k = 0;
                    int i = 0;

                    while (i < number!.Length)
                    {
                        int count = 0;
                        int j = 0;
                        while (j < number.Length)
                        {
                            if (number[i] == number[j])
                            {
                                count++;
                            }
                            j++;
                        }
                        if (count != 2)
                        {
                            integer[k] = number[i];
                            k++;
                        }
                        i++;
                    }
                    return Ok(integer);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("NumSquares")]
        public IActionResult NumSquares(NumSquaresClass numSquares)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = numSquares.Number;
                    if (IsSquare(number))
                    {
                        return Ok(1);
                    }
                    int k = 0;
                    // x = 4^k*(8*m + 7) => 4
                    while (k <= Sqrt(number) / 2)
                    {
                        for (int m = 0; m < number; m++)
                        {
                            int r = Power(4, k) * (8 * m + 7);
                            if (r > number)
                            {
                                continue;
                            }

                            if (r == number)
                            {
                                return Ok(4);
                            }
                        }
                        k++;
                    }

                    // x - i^2 = a^2 => 2
                    int i = 0;
                    while (i <= Sqrt(number))
                    {
                        if (IsSquare(number - (i * i)))
                        {
                            return Ok(2);
                        }
                        i++;
                    }


                    return Ok(3);
                }


            }
            catch (Exception)
            {
                throw;
            }
            // x = a^2

        }
        private static bool IsSquare(int number)
        {
            int y = Sqrt(number);
            return y * y == number;
        }
        private static int Sqrt(int number)
        {
            if (number == 0)
            {
                return 0;
            }

            int left = 1, right = number;
            while (true)
            {
                int mid = left + (right - left) / 2;
                if (mid > number / mid)
                {
                    right = mid - 1;
                }
                else
                {
                    if (mid + 1 > number / (mid + 1))
                    {
                        return mid;
                    }

                    left = mid + 1;
                }
            }
        }
        private static int Power(int number, int number2)
        {
            int result = 1;
            while (number2 > 0)
            {
                if ((number2 & 1) == 1)
                {
                    result *= number;
                }
                number *= number;
                number2 >>= 1;
            }
            return result;
        }
        //-----------   This Program is used in Assignment Operator     ------------

        //[HttpPost, Route("nextPermutation")]                            // [ 1, 2, 3] = [ 1, 3, 2 ]
        //public IActionResult NextPermutation(NextPermutationClass nextPermutation)
        //{
        //    int[]? number = nextPermutation.Number;
        //    int i = number!.Length - 1;
        //    while (i > 0 && number[i - 1] >= number[i])
        //        i--;

        //    if (i <= 0)
        //    {
        //        Array.Reverse(number);
        //        return Ok();
        //    }

        //    int j = number.Length - 1;
        //    while (j >= 0 && number[j] <= number[i - 1])
        //        j--;

        //    (number[i - 1], number[j]) = (number[j], number[i - 1]);
        //    Array.Reverse(number, i, number.Length - i);
        //    return Ok(number);
        //}
    }
}
