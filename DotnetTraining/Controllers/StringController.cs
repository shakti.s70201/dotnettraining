﻿using DotnetTraining.Models.ModelsString;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Text;

namespace DotnetTraining.Controllers
{
    [Route("api/string/")]
    public class StringController : Controller
    {

        [HttpPost, Route("uniqeLongestSubString")]
        public ActionResult<string> String1(StringValue stringValue)
        {
            string value = stringValue.Values;
            value = value.ToLower();
            string collect = string.Empty;

            List<string> list = new();

            foreach (var item in value)
            {
                if (!collect.Contains(item))
                {
                    collect += item;
                }
                else
                {
                    list.Add(collect);
                    collect = Convert.ToString(item);
                }
            }
            int max = 0;
            foreach (var item in list)
            {
                if (max == item.Length) { collect += $"/n{max} - {item}"; }
                if (max < item.Length)
                {
                    collect = item; max = item.Length;
                }
            }
            return $"{max} - {collect}";
        }

        //[HttpGet, Route("api/String2_")]
        //public ActionResult<int> String2(string inputstring, int target)
        //{


        //string[] str = inputstring.Split("[.]");
        //List<string> list = new();
        //for (int i = str.Length - 1; i >= 0; i--)
        //{
        //    list.Add(str[i]);
        //}
        //string[]? str1 = list.ToArray(string[0]);
        //string rev = String.Join(".", str1);
        //return rev;


        //    string input = inputstring;
        //    int number = target;
        //    int count = 0;
        //    for (int i = 0; i < number; i++)
        //    {
        //        for (int j = i + 1; j < number; j++)
        //        {
        //            if (isPallidrome(input, i, j))
        //                count++;
        //        }
        //    }
        //    return count;
        //    bool isPallidrome(string s, int i, int j)
        //    {
        //        if (i > j) return false;
        //        while (i < j)
        //        {
        //            if (s[i] != s[j])
        //            {
        //                return false;
        //            }
        //            i++;
        //            j--;
        //            Console.WriteLine(s[i] + s[j]);
        //        }
        //        return true;
        //    }
        //}




        [HttpPost, Route("api/Valid_Palindrome")] 
        public IActionResult PalindroneString(StringValue stringValue)
        {
            string value = stringValue.Values ?? " ";
            int left = 0, right = value.Length - 1;
            while (left < right)
            {
                while (left < right && !char.IsLetterOrDigit(value[left]))
                {
                    left++;
                }
                while (left < right && !char.IsLetterOrDigit(value[right]))
                {
                    right--;
                }
                if (char.ToLower(value[left]) != char.ToLower(value[right]))
                {
                    return Ok(false);
                }

                left++; 
                right--;
            }
            return Ok(true);
        }




        [HttpPost, Route("Reverse_Words_in_a_String")]
        public IActionResult ReverseWord(ReverseWordClass reverseWord)
        {
            StringBuilder value = new(reverseWord.StringValues);

            if (value == null || value.Length == 0) return Ok("");

            int j = value.Length;
            StringBuilder reversed = new();
            for (int i = value.Length - 1; i >= 0; i--)
            {
                if (value[i] == ' ')
                {
                    j = i;
                }
                else if (i == 0 || value[i - 1] == ' ')
                {
                    if (reversed.Length != 0)
                    {
                        reversed.Append(' ');
                    }
                    reversed.Append(value.ToString()[i..j]);
                }
            }
            return Ok(reversed.ToString());
        }




        [HttpPost, Route("Find_the_Index_of_the_First_Occurrence_in_a_String")]
        public IActionResult FirstOccurrence(FirstOccurrenceClass firstOccurrence)
        {
            string? value = firstOccurrence.Values;
            string? target = firstOccurrence.Target;
            if (value == null || target == null)
            {
                return Ok(-1);
            }
            int m = value.Length, n = target.Length;
            if (n == 0) return Ok(0);
            if (m < n) return Ok(-1);
            for (int i = 0; i < m - n + 1; i++)
            {
                int j;
                for (j = 0; j < n; j++)
                {
                    if (value[i + j] != target[j])
                        break;
                }
                if (j == n) return Ok(i);
            }
            return Ok(-1);
            // return  Ok(value.IndexOf(target));
        }




        [HttpPost, Route("decodeString")]
        public IActionResult DecodeString(DecodeStringClass decodeString)
        {
            string? value = decodeString.StringValues;
            //string value = new("2[abc]3[cd]ef");
            string collect = string.Empty;
            StringBuilder result = new();
            bool check = false;
            int number = 0;
            for (int i = 0; i < value!.Length; i++)
            {
                Console.WriteLine(value[i]);
                if (Char.IsDigit(value[i]))
                {
                    check = true; number = Convert.ToInt32(value[i].ToString()); continue;
                }
                if (check == true)
                {
                    if (value[i] == '[') { continue; }
                    else if (value[i] == ']')
                    {
                        result.Append(string.Concat(Enumerable.Repeat(collect, number)));
                        collect = "";
                        number = 0;
                        check = false;
                        continue;
                    }
                    else if (i == (value.Length - 1))
                    {
                        collect += value[i]; 
                        result.Append(string.Concat(Enumerable.Repeat(collect, number)));
                        continue;
                    }
                    else { collect += value[i]; continue; }
                }
                result.Append(value[i]);
            }
            return Ok(result.ToString());
        }

    }
}
