﻿using DotnetTraining.Models.Operator.Conditional.IfElseLadder;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/conditional/if_Else_Ladder/")]
    public class If_ElseControler : Controller
    {


        [HttpPost, Route("TruncatablePrime")]            // If Else-If Ladder    
        public IActionResult TruncatablePrime(TruncatablePrimeCLass truncatablePrime)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    //int number = 3797;  
                    int number = truncatablePrime.Number;
                    if (TruncatablePrime(number) == true)
                    {
                        return Ok(number + " is a truncatable prime.");
                    }
                    else
                    {
                        return Ok(number + " is not a truncatable prime.");
                    }
                    static bool PrimeMethod(int number)
                    {
                        if (number <= 1) return false;
                        else if (number == 2) return true;
                        else if (number % 2 == 0) return false;
                        else
                        {
                            for (int i = 3; i <= Math.Sqrt(number); i += 2)
                            {
                                if (number % i == 0) return false;
                            }
                        }
                        return true;
                    }
                    static bool TruncatablePrime(int n)
                    {
                        // checking from left-Side prime or not.
                        int leftSide = n;
                        while (leftSide > 0)
                        {
                            if (PrimeMethod(leftSide) != true)
                            {
                                return false;
                            }
                            else
                            {
                                leftSide /= 10;
                            }

                        }
                        // checking from right-Side prime or not.
                        int rightSide = 0;
                        int pow = 1;
                        while (rightSide == n)
                        {
                            if (!PrimeMethod(rightSide))
                            {
                                return false;
                            }
                            else
                            {
                                pow *= 10;
                                rightSide = (n %= pow);
                            }
                        }
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("validParentheses")]                        // If Else-If Ladder
        public ActionResult<bool> ValidParentheses(ValidParenthesesCLass validParentheses)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    //string[] valuearray = { "()", "[]", "{}" };

                    string? brackets = validParentheses.BracketsValue;

                    if (brackets == null || brackets.Length == 0)
                    {
                        return false;
                    }
                    if (brackets.Length % 2 == 1)
                    {
                        return false;
                    }
                    Stack<char> stack = new();

                    foreach (char c in brackets)
                    {

                        if (c == '(')
                        {
                            stack.Push(')');
                        }
                        else if (c == '[')
                        {
                            stack.Push(']');
                        }
                        else if (c == '{')
                        {
                            stack.Push('}');
                        }
                        else if (stack.Count == 0 || stack.Pop() != c)
                        {
                            return false;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    return stack.Count == 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost,Route("fizzBuzz")]
        public IActionResult FizzBuzz(FizzBuzzClass fizzBuzz)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = fizzBuzz.Number;
                    List<string> list = new();
                    for (int i = 1; i <= number; i++)
                    {
                        if (i % 15 == 0)
                        {
                            list.Add("FizzBuzz");
                        }
                        else if (i % 3 == 0)
                        {
                            list.Add("Fizz");
                        }
                        else if (i % 5 == 0)
                        {
                            list.Add("Buzz");
                        }
                        else
                        {
                            list.Add(i.ToString());
                        }
                    }
                    return Ok(list);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("searchInRotatedSortedArray")]
        public IActionResult SearchInRotatedSortedArray(SearchInRotatedSortedArrayClass searchInRotated)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = searchInRotated.Number;
                    int target = searchInRotated.Target;
                    int low = 0, high = number!.Length - 1;
                    int middle, lowValue, highValue, middleValue;

                    while (low <= high)
                    {
                        lowValue = number[low];
                        highValue = number[high];
                        if (lowValue <= highValue && (target < lowValue || target > highValue))
                        {
                            return Ok(-1);
                        }

                        middle = low + (high - low) / 2;
                        middleValue = number[middle];
                        if (target == middleValue) { return Ok(middle); }

                        if (lowValue <= middleValue)
                        {
                            if (lowValue <= target && target < middleValue)
                            {
                                high = middle - 1;
                            }
                            else
                            {
                                low = middle + 1;
                            }
                        }
                        else
                        {
                            if (target <= highValue && middleValue < target)
                            {
                                low = middle + 1;
                            }
                            else
                            {
                                high = middle - 1;
                            }
                        }
                    }

                    return Ok(-1);

                    //One Line Code
                    //return Ok((Array.IndexOf(number, target) != -1) ? Array.IndexOf(number, target) : -1);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("_Permutation")]
        public IActionResult Permutation(int[] nums)
        {
            List<List<int>> list = new();
            List<int> list1 = new();
            if (nums.Length == 1)
            {
                list1.Add(nums[0]);
                list.Add(list1);
                return Ok(list);
            }
            else if (nums.Length == 2)
            {
                list1.AddRange(nums[0..^0]);
                list.Add(list1);
                list1.Clear();
                list1.Add(nums[1]);
                list1.Add(nums[0]);
                list.Add(list1);
                list1.Clear();
                return Ok(list);
            }
            else
            {
                for (int i = 0; i < nums.Length; i++)
                {

                    for (int j = 0; j < nums.Length - 1; j++)
                    {
                        List<int> list2 = new();

                        if (i == 0)
                        {
                            list2.AddRange(nums[(j + 1)..^0]);
                            list2.AddRange(nums[0..(j + 1)]);
                        }
                        else
                        {
                            list2.AddRange(nums[j..^0]);
                            list2.AddRange(nums[0..j]);
                        }
                        list2.Remove(nums[i]);
                        list2.Insert(0, nums[i]);
                        list.Add(list2);
                    }
                }
            }
            return Ok(list);
        }

    }



    
}
