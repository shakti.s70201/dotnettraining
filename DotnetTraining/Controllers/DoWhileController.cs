﻿using DotnetTraining.Models.Loops;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/Loops/DoWhileLoop/")]
    public class DoWhileController : Controller
    {
        [HttpPost, Route("Perfect_Number")]                             //Return Bool Value
        public IActionResult PerfectNumber(PerfectNumberClass perfectNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = perfectNumber.Number;
                    int total = 0;
                    bool result = false;
                    int check = 0;
                    do
                    {
                        check++;
                        if (number % check == 0)
                        {
                            total += check;
                        }
                    }

                    while (check < number);
                    if (number == total)
                    {
                        result = true;
                    }
                    return Ok(result);

                    //int number = perfectNumber.Number;
                    //int sum = 0;

                    //for (int i = 1; i <= number / 2; i++)
                    //{
                    //    if (number % i == 0) sum += i;
                    //}

                    //return Ok(sum == number);
                }


            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("Majority_Element")]
        public IActionResult MajorityElement(MajorityElementClass majorityElement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    List<int> listValue = new();
                    listValue.AddRange(majorityElement.Number!);
                    int max = 0;
                    int majorityValue = 0;
                    int number = 0;
                    do
                    {
                        int value = listValue[number];
                        int count = listValue.FindAll(x => x == listValue[number]).Count;
                        if (count > max)
                        {
                            max = count;
                            majorityValue = listValue[number];
                        }
                        if (count > 1) { listValue.RemoveAll(x => x == value); }
                        number++;
                    } while (number < listValue.Count);
                    return Ok(majorityValue);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("pascalsTriangle")]
        public IActionResult PascalsTriangle(PascalsTriangleClass pascals)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int numberRows = pascals.NumberRows;
                    List<IList<int>> results = new();
                    if (numberRows == 0) return Ok(results);
                    results.Add(new List<int>(1) { 1 });
                    if (numberRows == 1) return Ok(results);
                    results.Add(new List<int>(2) { 1, 1 });
                    if (numberRows == 2) return Ok(results);

                    int i = 2;
                    do
                    {
                        IList<int> result = new List<int>() { 1 };

                        for (var j = 1; j < i; j++)
                        {
                            var number = results[i - 1][j - 1] + results[i - 1][j];
                            result.Add(number);
                        }

                        result.Add(1);
                        results.Add(result);

                        i++;
                    } while (i < numberRows);

                    return Ok(results);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("numberComplement")]
        public IActionResult NumberComplement(NumberComplementClass numberComplement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = numberComplement.ComplementNumber;
                    var result = 0;
                    var j = 0;
                    do
                    {
                        if (number % 2 == 0)
                        {
                            result += (int)Math.Pow(2, j);
                        }

                        j++;
                        number = (int)Math.Floor((double)(number / 2));

                    } while (number >= 1);

                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("commonFactors")]
        public IActionResult CommonFactors(CommonFactorsClass commonFactors)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number1 = commonFactors.Number1;
                    int number2 = commonFactors.Number2;

                    int max = Math.Max(number1, number2);
                    int counter = 0;

                    int i = 1;
                    do
                    {
                        if (number1 % i == 0 && number2 % i == 0)
                        {
                            counter++;
                        }

                        i++;

                    } while (i <= max);

                    return Ok(counter);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
