﻿using DotnetTraining.Models.Operator.Ternary;
using Microsoft.AspNetCore.Mvc;
using System;

namespace DotnetTraining.Controllers
{
    [Route("api/operator/ternary/")]
    public class TernaryController : Controller
    {
        [HttpPost, Route("clumsyFactorial")]
        public IActionResult ClumsyFactorial(ClumsyFactorialClass clumsyFactorial)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = clumsyFactorial.FactorialNumber;
                    return Ok(number == 1 ? 1 :
                   number == 2 ? 2 :
                   number == 3 ? 6 :
                   number == 4 ? 7 :
                   number % 4 == 1 ? number + 2 :
                   number % 4 == 2 ? number + 2 :
                   number % 4 == 3 ? number - 1 :
                   number + 1);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }


        [HttpPost, Route("compareVersionNumbers")]
        public IActionResult CompareVersionNumbers(VersionNumberClass versionNumbers)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    var nums1 = versionNumbers.Version1?.Split(new char[] { '.' });
                    var nums2 = versionNumbers.Version2?.Split(new char[] { '.' });

                    int length1 = nums1!.Length;
                    int length2 = nums2!.Length;

                    for (int i = 0; i < Math.Max(length1, length2); i++)
                    {
                        var current1 = i < length1 ? int.Parse(nums1[i]) : 0;
                        var current2 = i < length2 ? int.Parse(nums2[i]) : 0;
                        if (current1 != current2)
                        {
                            return Ok(current1 > current2 ? 1 : -1);
                        }

                    }

                    return Ok(0);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("subsetII")]
        public IActionResult SubsetII(SubsetIIClass subset)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = subset.SubsetArray;
                    List<IList<int>> results = new() { new List<int>() };

                    if (number!.Length == 0)
                    {
                        return Ok(results);
                    }

                    Array.Sort(number);

                    results.Add(new List<int>() { number[0] });
                    IList<IList<int>> lastAdded = new List<IList<int>>() { new List<int>() { number[0] } };

                    for (var i = 1; i < number.Length; i++)
                    {
                        List<IList<int>> result = new();
                        result.AddRange(number[i] == number[i - 1] ?
                            lastAdded.Select(item => { List<int> newItem = new(item) { number[i] }; return newItem; }) :
                            results.Select(item => { List<int> newItem = new(item) { number[i] }; return newItem; }));
                        results.AddRange(result);
                        lastAdded = result;
                    }

                    return Ok(results);
                }


            }
            catch (Exception)
            {
                throw;
            }

        }



        [HttpPost, Route("countOperations")]
        public IActionResult CountOperations(CountOperationsClass countOperations)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number1 = countOperations.Number1;
                    int number2 = countOperations.Number2;
                    if (number1 == 0 || number2 == 0)
                    { 
                        return Ok(0); 
                    }
                    int large = number1 >= number2 ? number1 : number2; 
                    int small = number1 < number2 ? number1 : number2; 
                    int count = 0; bool greater; 
                    while (large - small >= 0) 
                    {
                        count++;
                        greater = large - small >= small;
                        number1 = greater ? large - small : small;
                        number2 = greater ? small : large - small;
                        large = number1; 
                        small = number2;
                        if (small == 0) 
                        {
                            return Ok(count); 
                        } 
                    }
                    return Ok(0);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("addBinary")]
        public IActionResult AddBinary(AddBinaryClass addBinary)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? binary1 = addBinary.BinaryNumber1;
                    string? binary2 = addBinary.BinaryNumber2;
                    int carry = 0;
                    string result = "";
                    int i = binary1!.Length - 1, j = binary2!.Length - 1;

                    while (i >= 0 || j >= 0 || carry > 0)
                    {
                        int sum = carry;
                        sum += i >= 0 ? binary1![i--] - '0' : 0;
                        sum += j >= 0 ? binary2[j--] - '0' : 0;

                        result = (sum % 2 == 0 ? "0" : "1") + result;
                        carry = sum / 2;
                    }

                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }

    }
}
