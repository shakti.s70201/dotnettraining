﻿using DotnetTraining.Models.Unboxing;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace DotnetTraining.Controllers
{
    [Route("api/unboxing/")]
    public class UnboxingController : ControllerBase
    {
        [HttpPost, Route("NumPairsDivisibleBy60")]
        public IActionResult NumPairsDivisibleBy60(NumPairsDivisibleBy60Class numPairsDivisible)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[] value = new int[60];
                    object varia = value;
                    object? time = numPairsDivisible.Time;
                    CalculateModuloCount(time!, varia);
                    int res = CalculatePairs(varia);
                    res += CalculateSpecialCases(varia);
                    return Ok(res);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        static void CalculateModuloCount(object time, object varia)
        {
            int[] localvaria = (int[])varia;
            int[] localtime = (int[])time;
            foreach (int item in localtime)
            {
                localvaria[item % 60]++;
            }
        }
        static int CalculatePairs(object varia)
        {
            int[] localvaria = (int[])varia;
            int res = 0;
            for (int i = 1, j = 59; i < j; i++, j--)
            {
                if (localvaria[i] != 0 && localvaria[j] != 0)
                {
                    res += localvaria[i] * localvaria[j];
                }
            }
            return res;
        }
        static int CalculateSpecialCases(object varia)
        {
            int[] localvaria = (int[])varia;
            int res = 0;
            res += CalculatePairCombinations(localvaria[30] - 1);
            res += CalculatePairCombinations(localvaria[0] - 1);
            return res;
        }
        static int CalculatePairCombinations(int number)
        {
            return number * (number + 1) / 2;
        }




        [HttpPost, Route("countPrimes")]
        public IActionResult CountPrimes(CountPrimesClass countPrimes)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = countPrimes.Number;
                    object box = number;
                    bool[] prime = SieveOfEratosthenes(box);
                    int count = 0;
                    for (int i = 2; i < number; i++)
                    {
                        if (prime[i] == true) count++;
                    }
                    return Ok(count);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        static bool[] SieveOfEratosthenes(object box)
        {
            int number = (int)box;
            bool[] prime = new bool[number];
            for (int i = 0; i < number; i++)
            {
                prime[i] = true;
            }
            for (int p = 2; p * p < number; p++)
            {
                if (prime[p] == true)
                {
                    for (int i = p * p; i < number; i += p)
                    {
                        prime[i] = false;
                    }
                }
            }
            return prime;
        }




        [HttpPost, Route("maxNumberOfBalloons")]
        public IActionResult MaxNumberOfBalloons(MaxNumberOfBalloonsClass maxNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    object? text = maxNumber.Text;
                    Dictionary<char, int> dict = CountCharacters(text!);
                    dict['o'] = dict['o'] / 2;
                    dict['l'] = dict['l'] / 2;
                    int answer = dict.Values.Min();
                    return Ok(answer);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        static Dictionary<char, int> CountCharacters(object text)
        {
            string localText = (string)text;
            string balloon = "balloon";
            HashSet<char> balloonChars = new();
            Dictionary<char, int> dict = new();
            foreach (char c in balloon)
            {
                balloonChars.Add(c);
                dict[c] = 0;
            }
            foreach (char c in localText)
            {
                if (balloonChars.Contains(c))
                {
                    if (dict.ContainsKey(c))
                    {
                        dict[c]++;
                    }
                    else
                    {
                        dict.Add(c, 1);
                    }
                }
            }
            return dict;
        }




        [HttpPost, Route("longestIdealString")]
        public IActionResult LongestIdealString(LongestIdealStringClass longestIdeal)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? stringValue = longestIdeal.StringValue;
                    int intValue = longestIdeal.IntValue;
                    int n = stringValue!.Length;
                    int[] counts = new int[26];
                    for (int i = 0; i < n; i++)
                    {
                        int c = stringValue![i] - 'a';
                        int sub = c - intValue;
                        object subtract = sub;
                        int prev = GetMaxCount(counts, subtract, c + intValue);

                        counts[c] = 1 + prev;
                    }
                    int a = 0;
                    object zero = a;
                    return Ok(GetMaxCount(counts, zero, 25));
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        static int GetMaxCount(int[] counts, object subtract, int end)
        {
            int start = (int)subtract;
            int maxCount = 0;
            for (int j = start; j <= end; j++)
            {
                if (j >= 0 && j <= 25)
                {
                    maxCount = Math.Max(maxCount, counts[j]);
                }
            }
            return maxCount;
        }




        [HttpPost, Route("removeKdigits")]
        public IActionResult RemoveKdigits(RemoveKdigitsClass removeKdigits)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? value = removeKdigits.StringValue;
                    int target = removeKdigits.Target;
                    if (value!.Length <= target)
                    {
                        return Ok("0");
                    }
                    var stackValue = new Stack<char>();
                    object num = value;
                    stackValue = RemoveKdigitsHelper(num, target, stackValue);

                    var res = new StringBuilder();
                    while (stackValue.Count > 0) res.Insert(0, stackValue.Pop());
                    while (res.Length > 0 && res[0] == '0')
                        res.Remove(0, 1);

                    return Ok(res.Length == 0 ? "0" : res.ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        static Stack<char> RemoveKdigitsHelper(object number, int target, Stack<char> stackValue)
        {
            string localNumber = (string)number;
            for (int i = 0; i < localNumber.Length; i++)
            {
                var temp = localNumber[i];
                while (stackValue.Count > 0 && target > 0 && stackValue.Peek() > temp)
                {
                    stackValue.Pop(); target--;
                }
                stackValue.Push(temp);
            }
            while (target > 0) { stackValue.Pop(); target--; }
            return stackValue;
        }
    }
}
