﻿using DotnetTraining.Models.Operator.Comparision;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace DotnetTraining.Controllers
{
    [Route("api/operator/Comparison/")]
    public class ComparisionController : Controller
    {
        [HttpPost, Route("fourSum")]
        public IActionResult FourSum(FourSumClass fourSum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = fourSum.FourSumArray;
                    int target = fourSum.Target;

                    List<IList<int>> results = new();
                    if (number == null || number.Length < 4)
                    {
                        return Ok(results);
                    }

                    Array.Sort(number);

                    for (var i = 0; i < number.Length - 3; i++)
                    {
                        if (i > 0 && number[i] == number[i - 1])
                        {
                            continue;
                        }

                        for (var j = i + 1; j < number.Length - 2; j++)
                        {
                            if (j > i + 1 && number[j] == number[j - 1])
                            {
                                continue;
                            }

                            long sum = number[i] + number[j];
                            var left = j + 1;
                            var right = number.Length - 1;

                            while (left < right)
                            {
                                long total = sum + number[left] + number[right];

                                if (total == target)
                                {
                                    results.Add(new List<int> { number[i], number[j], number[left], number[right] });
                                    left++;
                                    right--;
                                    while (left < right && number[left] == number[left - 1]) left++;
                                    while (left < right && number[right] == number[right + 1]) right--;
                                }
                                else if (total < target)
                                {
                                    left++;
                                }
                                else
                                {
                                    right--;
                                }
                                   

                            }
                        }
                    }
                    return Ok(results);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }


        [HttpPost, Route("addStrings")]
        public IActionResult AddStrings(AddStringsClass addStrings)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? number1 = addStrings.Number1;
                    string? number2 = addStrings.Number2;
                    var sb = new StringBuilder();
                    int i = number1!.Length - 1, j = number2!.Length - 1, carry = 0;
                    while (i >= 0 && j >= 0)
                    {
                        var value = number1[i--] - '0' + number2[j--] - '0' + carry;

                        var digit = value % 10;
                        carry = value / 10;

                        sb.Append(digit);
                    }

                    while (i >= 0)
                    {
                        var value = number1[i--] - '0' + carry;
                        var digit = value % 10;
                        carry = value / 10;
                        sb.Append(digit);
                    }

                    while (j >= 0)
                    {
                        var value = number2[j--] - '0' + carry;
                        var digit = value % 10;
                        carry = value / 10;
                        sb.Append(digit);
                    }

                    if (carry != 0)
                        sb.Append(carry);
                    string result = new(sb.ToString().Reverse().ToArray());
                    return Ok(result);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("isIsomorphic")]
        public IActionResult IsIsomorphic(IsIsomorphicClass isIsomorphic)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? stringValue = isIsomorphic.StringValue;
                    string? target = isIsomorphic.Target;
                    if (stringValue!.Length != target!.Length) return Ok(false);

                    var map = new Dictionary<char, char>(26);
                    var visited = new HashSet<char>(26);
                    for (int i = 0; i < stringValue.Length; i++)
                    {
                        if (map.ContainsKey(stringValue[i]))
                        {
                            if (map[stringValue[i]] != target[i]) return Ok(false);
                        }
                        else
                        {
                            if (visited.Contains(target[i]))
                            {
                                return Ok(false);
                            }
                            
                            map.Add(stringValue[i], target[i]);
                            visited.Add(target[i]);
                        }
                    }
                    return Ok(true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("addToArrayFormOfInteger")]
        public IActionResult AddToArrayForm(AddToArrayFormClass addToArray)
        {
            int[]? array = addToArray.IntegerArray;
            int integer = addToArray.IntegerValue;
            var result = new List<int>();
            var carry = 0;

            var i = array!.Length - 1;
            while (i >= 0 && integer > 0)
            {
                var sum = array![i--] + integer % 10 + carry;
                integer /= 10;

                var digit = sum % 10;
                carry = sum / 10;
                result.Add(digit);
            }

            while (i >= 0)
            {
                var sum = array![i--] + carry;
                var digit = sum % 10;
                carry = sum / 10;
                result.Add(digit);
            }

            while (integer > 0)
            {
                var sum = integer % 10 + carry;
                integer /= 10;
                var digit = sum % 10;
                carry = sum / 10;
                result.Add(digit);
            }

            if (carry > 0)
            {
                result.Add(carry);
            }

            result.Reverse();
            return Ok(result);
        }



        [HttpPost, Route("divideTwoIntegers")]
        public IActionResult DivideTwoIntegers(DivideTwoIntegersClass twoIntegersClass)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int divisor = twoIntegersClass.Divisor;
                    int dividend = twoIntegersClass.Dividend;

                    if (divisor == 0) { throw new DivideByZeroException(); }
                    uint UnitDivident = (uint)(dividend < 0 ? -dividend : dividend);
                    uint UnitDivisor = (uint)(divisor < 0 ? -divisor : divisor);

                    uint result = 0, currentDivisor = 0;
                    var index = 0;
                    while (UnitDivident >= UnitDivisor)
                    {
                        currentDivisor = UnitDivisor;
                        for (index = 0; UnitDivident >= currentDivisor && currentDivisor != 0; index++, currentDivisor *= 2)
                        {
                            UnitDivident -= currentDivisor;
                            result += (uint)1 << index;
                        }
                    }

                    return Ok((dividend ^ divisor) >> 31 == -1
                        ? (int)-result
                        : result > int.MaxValue ? int.MaxValue : (int)result);
                }
            }
            catch (Exception)
            {
                throw;
            }

            
        }
    }
    
}
