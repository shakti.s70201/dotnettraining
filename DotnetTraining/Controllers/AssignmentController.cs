﻿
using DotnetTraining.Models.Operator.Assignment;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/operator/assignment/")]
    public class AssignmentController : Controller
    {
        [HttpPost, Route("nextPermutation")]                            // [ 1, 2, 3] = [ 1, 3, 2 ]
        public IActionResult NextPermutation(NextPermutationClass nextPermutation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = nextPermutation.Number;
                    int i = number!.Length - 1;
                    while (i > 0 && number[i - 1] >= number[i])
                        i--;

                    if (i <= 0)
                    {
                        Array.Reverse(number);
                        return Ok();
                    }

                    int j = number.Length - 1;
                    while (j >= 0 && number[j] <= number[i - 1])
                        j--;

                    (number[i - 1], number[j]) = (number[j], number[i - 1]);
                    Array.Reverse(number, i, number.Length - i);
                    return Ok(number);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("integerReplacement")]
        public IActionResult IntegerReplacement(IntegerReplacementClass integerReplacement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int count = 0;
                    long number = integerReplacement.Number;
                    while (number != 1)
                    {
                        if (number % 2 == 0)
                        {
                            number /= 2;
                        }
                        else if (number == 3 || (number >> 1) % 2 == 0)
                        {
                            number--;
                        }
                        else
                        {
                            number++;
                        }
                        count++;
                    }
                    return Ok(count);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("fibonacciNumber")]
        public IActionResult FibonacciNumber(FibonacciNumberClass fibonacci)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = fibonacci.FibonacciNumber;
                    if (number == 0) return Ok(0);
                    if (number == 1) return Ok(1);

                    int prev1 = 0;
                    int prev2 = 1;
                    for (int i = 2; i < number; i++)
                    {
                        int temp = prev1 + prev2;
                        prev1 = prev2;
                        prev2 = temp;
                    }

                    return Ok(prev1 + prev2);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("numberOfStepsToReduceANumberToZero")]
        public IActionResult NumberOfSteps(NumberOfStepsClass numberSteps)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = numberSteps.Number;
                    var step = 0;
                    int result;
                    while (number > 0)
                    {
                        if ((number & 1) == 1)
                            step++;
                        number /= 2;
                        step++;
                    }
                    result = step > 0 ? step - 1 : 0;
                    return Ok(result);
                }

            }
            catch (Exception)
            {
                throw;
            }
            
        }


        [HttpPost, Route("simplifiedFractions")]
        public IActionResult SimplifiedFractions(SimplifiedFractionsClass fractions)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = fractions.Number;
                    List<string> list = new();
                    int count = 1, upon = 2;
                    while (count < number)
                    {
                        if (IsSimplified(count, upon))
                            list.Add($"{count}/{upon}");

                        if (upon >= number)
                        {
                            ++count;
                            upon = count + 1;
                        }
                        else
                            ++upon;
                    }

                    return Ok(list);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }
        static bool IsSimplified(int num, int den)
        {
            if (num == 1)
                return true;

            for (int i = num; i > 1; --i)
                if (num % i == 0 && den % i == 0)
                    return false;

            return true;
        }
    }
}
