﻿using DotnetTraining.Models.Operator.Conditional.SwitchCase;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/Conditional/SwitchCase/")]
    public class SwitchCaseController : Controller
    {

        [HttpPost, Route("valid_Phone_Number")]
        public IActionResult ValidPhoneNumber()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string digits = "23";
                    if (digits == null || digits.Length == 0)
                    {
                        return Ok(new List<string>());
                    }
                    Queue<string> queueValue = new();
                    queueValue.Enqueue("");

                    for (int i = 0; i < digits.Length; i++)
                    {

                        int num = digits[i] - '0';
                        while (queueValue.Peek().Length == i)
                        {
                            string preStr = queueValue.Dequeue();
                            foreach (char c in SwitchCaseFunction(num))
                            {
                                queueValue.Enqueue(preStr + c);
                            }
                        }
                    }
                    return Ok(new List<string>(queueValue));
                }


            }
            catch (Exception)
            {
                throw;
            }

        }
        static string SwitchCaseFunction(int values)
        {
            return values switch
            {
                0 => "0",
                1 => "1",
                2 => "abc",
                3 => "def",
                4 => "ghi",
                5 => "jkl",
                6 => "mno",
                7 => "pqrs",
                8 => "tuv",
                9 => "wxyz",
                _ => "",
            };
        }



        [HttpPost, Route("permutationsII")]
        public IActionResult PermutationsII(PermutationsIIClass permutationsII)
        {
            int[]? array = permutationsII.PermutationsIIArray;
            IList<IList<int>> result = new List<IList<int>>();
            Array.Sort(array!);
            Backtrack(array!, result, new List<int>(), new bool[array!.Length]);
            return Ok(result);
        }
        private void Backtrack(int[] nums, IList<IList<int>> result, List<int> permutation, bool[] used)
        {
            switch (permutation.Count == nums.Length)
            {
                case true:
                    result.Add(new List<int>(permutation));
                    break;
                case false:
                    for (int i = 0; i < nums.Length; i++)
                    {
                        switch (used[i] || (i > 0 && nums[i] == nums[i - 1] && !used[i - 1]))
                        {
                            case true:
                                continue;
                            case false:
                                permutation.Add(nums[i]);
                                used[i] = true;
                                Backtrack(nums, result, permutation, used);
                                used[i] = false;
                                permutation.RemoveAt(permutation.Count - 1);
                                break;
                        }
                    }
                    break;
            }
        }



        [HttpPost, Route("relativeRanks")]
        public IActionResult RelativeRanks(RelativeRanksClass relativeRanks)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? score = relativeRanks.Values;
                    string[] result = new string[score!.Length];
                    int[] scoreCp = new int[score.Length];
                    for (int i = 0; i < score.Length; i++)
                        scoreCp[i] = score[i];
                    Array.Sort(scoreCp);
                    Array.Reverse(scoreCp);
                    Dictionary<int, string> dict = new();

                    for (int i = 0; i < scoreCp.Length; i++)
                    {
                        dict[scoreCp[i]] = SwitchMethod(i);
                    }
                    for (int i = 0; i < score.Length; i++)
                        result[i] = dict[score[i]];
                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static string SwitchMethod(int number)
        {
            return number switch
            {
                0 => "Gold Medal",
                1 => "Silver Medal",
                2 => "Gold Medal",
                _ => $"{number + 1}",
            };
        }


        [HttpPost, Route("romanToInteger")]
        public IActionResult ConvertToInteger([FromBody] RomanToIntegerClass romanToInteger)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? roman = romanToInteger.RomanString;
                    var result = 0;
                    for (var i = roman!.Length - 1; i >= 0; i--)
                    {
                        switch (roman[i])
                        {
                            case 'I': result += 1; break;
                            case 'V' when i > 0 && roman[i - 1].Equals('I'): result += 4; i--; break;
                            case 'V': result += 5; break;
                            case 'X' when i > 0 && roman[i - 1].Equals('I'): result += 9; i--; break;
                            case 'X': result += 10; break;
                            case 'L' when i > 0 && roman[i - 1].Equals('X'): result += 40; i--; break;
                            case 'L': result += 50; break;
                            case 'C' when i > 0 && roman[i - 1].Equals('X'): result += 90; i--; break;
                            case 'C': result += 100; break;
                            case 'D' when i > 0 && roman[i - 1].Equals('C'): result += 400; i--; break;
                            case 'D': result += 500; break;
                            case 'M' when i > 0 && roman[i - 1].Equals('C'): result += 900; i--; break;
                            case 'M': result += 1000; break;
                        }
                    }
                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }



        [HttpPost, Route("twoValue")]                             //Switch Case
        public ActionResult<string> SwtchCase1(SwitchCase1 switchCase)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? values = switchCase.Values;
                    int target = switchCase.Target;
                    string? sign = switchCase.OperatorSign;
                    string showResult = "";
                    switch (sign)
                    {
                        case "+":
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target)
                                { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] > target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                { if (values[i] + values[j] == target) { showResult += $"position ({i}, {j}). values {values[i]} + {values[j]} = {target}\n"; } }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        case "-":
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target) { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] < target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                {
                                    if (values[i] > values[j])
                                    { if (values[i] - values[j] == target) { showResult += $"position ({i}, {j}). values {values[i]} - {values[j]} = {target}\n"; } }
                                    else
                                    { if (values[j] - values[i] == target) { showResult += $"position ({j}, {i}). values {values[j]} - {values[i]} = {target}\n"; } }
                                }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        case "*":
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target) { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] > target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                { if (values[i] * values[j] == target) { showResult += $"position ({i}, {j}). values {values[i]} * {values[j]} = {target}\n"; } }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        case "/":
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target)
                                { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] < target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                {
                                    if (values[i] > values[j])
                                    { if (values[i] / values[j] == target) { showResult += $"position ({i}, {j}). values {values[i]} / {values[j]} = {target}\n"; } }
                                    else
                                    { if (values[j] / values[i] == target) { showResult += $"position ({j}, {i}). values {values[j]} / {values[i]} = {target}\n"; } }
                                }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        default: return Ok("Please use this +, -, x, % Operator Only");
                    }
                Label: return Ok("No Matching Values Find");
                }


            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
