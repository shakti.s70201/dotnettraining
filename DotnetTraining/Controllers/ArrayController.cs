﻿using DotnetTraining.Models.Array;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/array/")]
    public class ArrayController : Controller
    {

        [HttpPost, Route("Minimum_Index_Sum_of_Two_Lists")]             //List Of Strings
        public IActionResult MinimumIndexSumOfTwoArray(SumOfTwoArrayClass sumOfTwoArrayClass)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                return Ok(FindRestaurant(sumOfTwoArrayClass.Array1!, sumOfTwoArrayClass.Array2!));

                string[] FindRestaurant(string[] list1, string[] list2)
                {
                    Dictionary<string, int> dict = new();

                    int number1 = 0;
                    while (number1 < list1.Length)
                    {
                        int number2 = 0;
                        while (number2 < list2.Length)
                        {
                            if (list1[number1] == list2[number2])
                            {
                                dict.Add(list1[number1], number1 + number2);
                            }
                            number2++;
                        }
                        number1++;
                    }

                    int min = dict.MinBy(x => x.Value).Value;
                    return dict.Where(x => x.Value == min).Select(x => x.Key).ToArray();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }


        [HttpPost, Route("findErrorNums")]
        public IActionResult FindErrorNums(FindErrorNumsClass findError)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = findError.Number;
                    HashSet<int> set = new();
                    int duplicate = -1, missing = -1;
                    List<int> list = new(number!);
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (set.Add(list[i]) == false)
                        {
                            duplicate = number![i];
                            break;
                        }
                    }
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list.Contains(i + 1) == false)
                        {
                            missing = i + 1;
                            break;
                        }
                    }
                    return Ok(new int[] { duplicate, missing });



                    //Three Line Code
                    //int first = number!.GroupBy(x => x).Where(g => g.Count() == 2).Select(x => x.Key).First();
                    //int second = Enumerable.Range(1, number!.Length).Except(number).First();
                    //return Ok(new int[] { first, second });
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("plusOne")]
        public IActionResult PlusOne(PlusOneClass plusOne)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? digits = plusOne.ArrayDigits;
                    if (digits!.Length == 0)
                    {
                        return Ok(digits);
                    }

                    if (digits[^1] + 1 <= 9)
                    {
                        digits[^1] += 1;
                        return Ok(digits);
                    }
                    int i = digits.Length - 1;
                    while (i >= 0 && digits[i] + 1 >= 10)
                    {
                        digits[i] = 0;
                        i--;
                    }
                    if (i >= 0)
                    {
                        digits[i] += 1;
                        return Ok(digits);
                    }
                    else
                    {
                        int[] result = new int[digits.Length + 1];
                        result[0] = 1;
                        Array.Copy(digits, 0, result, 1, digits.Length);
                        return Ok(result);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("pascalsTriangleII")]
        public IActionResult PascalsTriangleII(PascalsTriangleIICLass pascals)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int rowIndex = pascals.GetRows;
                    var result = new int[rowIndex + 1];
                    result[0] = 1;
                    if (rowIndex == 0) return Ok(result);

                    for (int i = 1; i <= rowIndex; i++)
                        for (int j = i; j > 0; j--)
                            result[j] = result[j - 1] + result[j];

                    return Ok(result);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("FourSumCount")]
        public IActionResult FourSumCount([FromBody] FourSumCountClass fourSum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number1 = fourSum.Number1;
                    int[]? number2 = fourSum.Number2;
                    int[]? number3 = fourSum.Number3;
                    int[]? number4 = fourSum.Number4;
                    int n = number1!.Length;
                    Dictionary<int, int> dict = new();
                    int count = 0;

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            int sum = number1[i] + number2![j];
                            if (dict.ContainsKey(sum))
                            {
                                dict[sum]++;
                            }
                            else
                            {
                                dict.Add(sum, 1);
                            }
                        }
                    }

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            int sum = number3![i] + number4![j];
                            if (dict.ContainsKey(-sum))
                            {
                                count += dict[-sum];
                            }
                        }
                    }

                    return Ok(count);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
