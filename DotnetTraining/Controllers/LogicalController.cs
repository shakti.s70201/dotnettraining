﻿using Microsoft.AspNetCore.Mvc;
using DotnetTraining.Models.Operator.Logical;

namespace DotnetTraining.Controllers
{
    [Route("api/Logical/")]
    public class LogicalController : Controller
    {
        [HttpPost, Route("threeSum")]
        public IActionResult ThreeSum(ThreeSumClass threeSum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? numbers = threeSum.Numbers;
                    Array.Sort(numbers!);
                    List<IList<int>> result = new();
                    for (int i = 0; i < numbers!.Length && numbers[i] <= 0; ++i)
                    {
                        if (i == 0 || numbers[i - 1] != numbers[i])
                        {
                            TwoSum(numbers, i, result);
                        }
                    }
                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        static void TwoSum(int[] numbers, int i, List<IList<int>> result)
        {
            int low = i + 1;
            int high = numbers.Length - 1;
            while (low < high)
            {
                int sum = numbers[i] + numbers[low] + numbers[high];
                if (sum < 0 || (low > i + 1 && numbers[low] == numbers[low - 1]))
                {
                    ++low;
                }
                else if (sum > 0 || (high < numbers.Length - 1 && numbers[high] == numbers[high + 1]))
                {
                    --high;
                }
                else
                {
                    result.Add(new List<int> { numbers[i], numbers[low++], numbers[high--] });
                }
            }
        }



        [HttpPost, Route("validParentheses")]
        public ActionResult<bool> ValidParentheses(ParenthesesClass parentheses)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    //string[] valuearray = { "()", "[]", "{}" };

                    string? brackets = parentheses.BracketsValue;

                    if (brackets == null || brackets.Length == 0)
                    {
                        return Ok(false);
                    }
                    if (brackets.Length % 2 == 1)
                    {
                        return Ok(false);
                    }
                    Stack<char> stack = new();

                    foreach (char c in brackets)
                    {

                        if (c == '(')
                        {
                            stack.Push(')');
                        }
                        else if (c == '[')
                        {
                            stack.Push(']');
                        }
                        else if (c == '{')
                        {
                            stack.Push('}');
                        }
                        else if (stack.Count == 0 || stack.Pop() != c)
                        {
                            return Ok(false);
                        }
                        else
                        {
                            continue;
                        }
                    }

                    return Ok(stack.Count == 0);
                }
            }
            catch (Exception)
            {
                throw;
            }
            


            //    bool result = true;
            //    for (int i = 0; i < valuearray.Length; i++)
            //    {
            //        string value = valuearray[i];
            //        if (brackets?.Count(t => t == value[0]) == brackets?.Count(t => t == value[1]) == false)
            //        {
            //            result = false;
            //            break;
            //        }
            //    }
            //    if (result == true)
            //    {
            //        for (int i = 0; i <= brackets?.Length; i++)
            //        {
            //            Console.WriteLine(brackets[i]);
            //            char value = brackets[i];
            //            if (value == '(') { if (!CheckBrackets(brackets, brackets.IndexOf(value), ')')) { result = false; break; }; }
            //            else if (value == '{') { if (!CheckBrackets(brackets, brackets.IndexOf(value), '}')) { result = false; break; }; }
            //            else if (value == '[') { if (!CheckBrackets(brackets, brackets.IndexOf(value), ']')) { result = false; break; }; }
            //            else if (value == ')') { result = false; break; }
            //            else if (value == '}') { result = false; break; }
            //            else if (value == ']') { result = false; break; }
            //            else { continue; }
            //            if (result == false) { break; }
            //        }
            //    }
            //    else { result = false; }
            //    return result;
            //    //void removeSubString(int i, int number)
            //    //{
            //    //    brackets?.Remove(i, 1);
            //    //    brackets?.Remove(number, 1);
            //    //}
            //}
            //static bool CheckBrackets(string brackets, int number, char charValue)
            //{
            //    bool result = true;
            //    for (int i = number + 1; i < brackets.Length; i++)
            //    {
            //        char value = brackets[i];
            //        Console.Write(value);
            //        if (value == charValue)
            //        {
            //            result = true;
            //            Console.WriteLine(i + "--" + number);
            //            brackets.Remove(i, 1);
            //            brackets.Remove(number, 1);
            //            break;
            //        }

            //        if (value == '(') { if (!CheckBrackets(brackets, brackets.IndexOf(value), ')')) { result = false; break; }; }
            //        else if (value == '{') { if (!CheckBrackets(brackets, brackets.IndexOf(value), '}')) { result = false; break; }; }
            //        else if (value == '[') { if (!CheckBrackets(brackets, brackets.IndexOf(value), ']')) { result = false; break; ; }; }
            //        else if (value == ')') { result = false; break; }
            //        else if (value == '}') { result = false; break; }
            //        else if (value == ']') { result = false; break; }
            //        else { continue; }
            //    }
            //    return result;
        }




        [HttpPost, Route("rotatedMirror")]
        public IActionResult RotateDigits([FromBody] RotatedMirrorCLass rotatedMirror)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int number = rotatedMirror.Number;
                int goodNumber = 0;

                for (int i = 1; i <= number; i++)
                {
                    string digitsNumber = FindDigitsNumber(i);
                    if (digitsNumber.Contains('3') || digitsNumber.Contains('4') || digitsNumber.Contains('7'))
                    {
                        continue;
                    }

                    else if (digitsNumber.Contains('2') || digitsNumber.Contains('5') || digitsNumber.Contains('6') || digitsNumber.Contains('9'))
                    {
                        goodNumber++;
                    }
                }
                return Ok(goodNumber);
            }

            catch (Exception)
            {
                throw;
            }
        }
        static string FindDigitsNumber(int number)
        {
            if (number == 0)
            {
                return "";
            }

            return FindDigitsNumber(number / 10) + (number % 10);
        }




        [HttpPost, Route("strobogrammaticNumber")]
        public IActionResult IsStrobogrammatic([FromBody] StroboGrammaticClass strobogrammatic)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                string? number = strobogrammatic.StringNumber;
                int i = 0;
                int j = number!.Length - 1;
                while (i <= j)
                {
                    if ((number[i] == '0' && number[j] == '0') ||
                    (number[i] == '1' && number[j] == '1') ||
                    (number[i] == '8' && number[j] == '8') ||
                    (number[i] == '6' && number[j] == '9') ||
                    (number[i] == '9' && number[j] == '6'))
                    {
                        i++;
                        j--;
                    }



                    else
                    {
                        return Ok(false);
                    }
                }



                return Ok(true);
            }



            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("reverseArrayPairs")]
        public IActionResult ReverseParis([FromBody] ReverseArrayPairsClass reverseArrayPairs)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }

                int[]? numberArray = reverseArrayPairs.NumberArray;
                return Ok(MergeSortAndCount(numberArray!, 0, numberArray!.Length - 1));

            }
            catch (Exception)
            {
                throw;
            }
        }
        static int MergeSortAndCount(int[] numbers, int start, int end)
        {
            if (start < end)
            {
                int middle = (start + end) / 2;
                int count = MergeSortAndCount(numbers, start, middle) + MergeSortAndCount(numbers, middle + 1, end);
                int j = middle + 1;
                for (int i = start; i <= middle; i++)
                {
                    while (j <= end && numbers[i] > numbers[j] * 2L)
                        j++;
                    count += j - (middle + 1);
                }



                Merge(numbers, start, middle, end);



                return count;
            }



            else
                return 0;
        }
        static void Merge(int[] numberArray, int start, int middle, int end)
        {
            int number1 = middle - start + 1;
            int number2 = end - middle;

            var left = new int[number1];
            var right = new int[number2];

#pragma warning disable IDE0059 // Unnecessary assignment of a value
            int i = 0;
#pragma warning restore IDE0059 // Unnecessary assignment of a value
#pragma warning disable IDE0059 // Unnecessary assignment of a value
            int j = 0;
#pragma warning restore IDE0059 // Unnecessary assignment of a value

            for (i = 0; i < number1; i++)
            {
                left[i] = numberArray[start + i];
            }

            for (j = 0; j < number2; j++)
            {
                right[j] = numberArray[middle + 1 + j];
            }




            for (int k = start; k <= end; k++)
            {
                if (j >= number2 || (i < number1 && left[i] <= right[j]))
                {
                    numberArray[k] = left[i++];
                }
                else
                {
                    numberArray[k] = right[j++];
                }
                    
            }
        }

    }

}
