﻿using DotnetTraining.Models.Operator.Bitwise;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/Bitwise/")]
    public class BitwiseController : Controller
    {


        [HttpPost, Route("Swap_Two_Variable")]                 //Using XOR
        public ActionResult<Array> BitwiseFirst(Bitwise bitwise)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? ints = bitwise.IntArrray;

                    for (int j = 0; j < ints?.Length - 1; j++)
                    {

                        for (int i = 0; i < ints?.Length - 1; i++)
                        {
                            if (ints?[i] > ints?[i + 1])
                            {
                                ints[i] = ints[i] ^ ints[i + 1];
                                ints[i + 1] = ints[i] ^ ints[i + 1];
                                ints[i] = ints[i] ^ ints[i + 1];
                            }
                        }
                    }
                    return ints ?? Array.Empty<int>();
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("Hamming_Distance")]                   //Using << left shift, and >> right shift.
        public IActionResult HammingDistance(HammingDistanceClass hammingDistance)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number1 = hammingDistance.Number1;
                    int number2 = hammingDistance.Number2;
                    int result = 0;

                    for (int i = 0; i < 31; i++)
                    {

                        if ((number1 & 1) != (number2 & 1))
                        {
                            result++;
                        }

                        number1 >>= 1;
                        number2 >>= 1;

                    }

                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("Bitwise_AND_Of_NumbersRange")]        // Using ~ and << Left shift
        public IActionResult Bitwise_AND_Of_NumbersRange(NumberRangeClass numberRange)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int left = numberRange.Left;
                    int right = numberRange.Right;

                    if (right == left)
                    {
                        return Ok(right);

                    }
                    
                    int shift = (int)Math.Log(right - left, 2) + 1;

                    shift = left & right & (~0 << shift);


                    return Ok(shift);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("findingMissingNumber")]               //Using XOR
        public IActionResult FindingMissingNumber(FindingMissingNumberClass bitwise)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    //int[] numbers = { 0, 1, 2, 3, 4 };
                    int[]? numbers = bitwise.IntArrray;
                    int collect = numbers!.Length;
                    for (int i = 0; i < numbers.Length; i++)
                    {
                        collect ^= numbers[i] ^ i;
                    }
                    return Ok(collect);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }





        [HttpPost, Route("singleNumberII")]                    // Using  ~, & and ^ bitwise operator 
        public IActionResult SingleNumberII(SingleNumberIIClass singleNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? arrayNumber = singleNumber.ArrayNumber;
                    int seenOnce = 0, seenTwice = 0;

                    foreach (int num in arrayNumber!)
                    {
                        seenOnce = ~seenTwice & (seenOnce ^ num);
                        seenTwice = ~seenOnce & (seenTwice ^ num);
                    }

                    return Ok(seenOnce);


                    //One Line Code
                    //return nums.GroupBy(p=>p).Select(g=>new { key = g.Key, cnt = g.Count()}).FirstOrDefault(p=>p.cnt == 1).key;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
