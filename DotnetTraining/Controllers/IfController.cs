﻿using DotnetTraining.Models.Operator.Bitwise;
using DotnetTraining.Models.Operator.Conditional.If;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/conditional/if/")]
    public class IfController : Controller
    {
        [HttpPost, Route("rangeMissingValue")]           //If only
        public ActionResult<string> RangeMissingValue(RangeMissingClass rangeMissing)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? numbers = rangeMissing.ArrayNumber;
                    int max = numbers!.Max();
                    int min = numbers!.Min();
                    string result = string.Empty;

                    for (int i = min; i < max; i++)
                    {
                        if (numbers!.Contains(i) != true) { result += $"{i}, "; }
                    }
                    if (result.Length == 0) { result += $"{numbers!.Length}, "; }
                    return $"( {result})";
                    //int[] nums = { 9, 6, 4, 2, 3, 5, 7, 0, 1 }; //if Range is continue
                    //int number = nums.Length;
                    //int expected = (number * (number + 1)) / 2;   //55
                    //int sum=0;
                    //foreach (int num in nums) { sum += num; }
                    //Console.WriteLine(expected - sum);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("stringToInteger")]             //If only
        public IActionResult StringToInteger(StringToIntegerClass stringToInteger)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? stringValue = stringToInteger.StringValue;
                    List<char> chars = new();
                    stringValue = stringValue!.TrimStart(' ');           //Input-Case ("   -42", "4193 with words","42)
                    for (int i = 0; i < stringValue.Length; i++)
                    {
                        if (Char.IsDigit(stringValue[i]))
                        {
                            chars.Add(stringValue[i]);
                            continue;
                        }
                        if (i == 0 && stringValue[0] == '-')
                            continue;
                        if (i == 0 && stringValue[0] == '+')
                            continue;
                        break;
                    }
                    if (chars.Count == 0) { return Ok(0); }

                    bool cor = int.TryParse(String.Join("", chars), out int result);
                    if (cor && stringValue[0] == '-') { return Ok(-result); }

                    if (cor) { return Ok(result); }

                    if (!cor && stringValue[0] == '-') { return Ok(int.MinValue); }

                    else
                    {
                        return Ok(int.MaxValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            

            //string s = "   -42";
            //int indexNo = 0;
            //string collect = string.Empty;
            //indexNo = s.IndexOf(collect);
            //for (int i = indexNo; i < s.Length; i++)
            //{
            //    if (s[i] == '0') { continue; }
            //    else if (s.Contains('+')) { collect += '+'; }
            //    else if (s.Contains('-')) { collect += '-'; }
            //    else if (char.IsDigit(s[i])) { collect += s[i]; }
            //}
            //if (int.Parse(collect) >= 2147483647) { collect = "2147483647"; }
            //return (int.Parse(collect));
        }




        [HttpPost, Route("squareRoot")]
        public IActionResult SquareRoot(SquareRootClass squareRoot)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int square = squareRoot.SqaureNumber;
                    if (square == 1)
                    {
                        return Ok(square);
                    }
                    for (double i = 0; i <= square; i++)
                    {

                        if (i * i > square)
                        {
                            return Ok(Convert.ToInt32(i - 1));
                        }
                    }
                    return Ok(0);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("singleNumber")]
        public IActionResult SingleNumber(SingleNumberClass single)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? number = single.SingleNumber;
                    Array.Sort(number!);

                    for (int i = 0; i < number!.Length; i += 2)
                    {
                        if (i == number.Length - 1)
                        {
                            return Ok(number[i]);
                        }

                        if (number[i] != number[i + 1])
                        {
                            return Ok(number[i]);
                        }
                    }


                    return Ok(0);




                    //OneLine Code
                    //return Ok(number.GroupBy(x => x).Where(g => g.Count() == 1).Select(x => x.Key).First());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }





        [HttpPost, Route("power")]                              //Using >> Right Swift
        public IActionResult Power(PowerClass power)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    double doubleValue = power.DoubleValue;
                    int number = power.Number;
                    double result = 1;
                    if (number < 0)
                    {
                        number = -number;
                        doubleValue = 1 / doubleValue;
                    }

                    while (number != 0)
                    {
                        if ((number & 1) != 0)
                        {
                            result *= doubleValue;
                        }

                        doubleValue *= doubleValue;
                        number = (int)((uint)number >> 1);
                    }
                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
