﻿using DotnetTraining.Models.Loops;
using Microsoft.AspNetCore.Mvc;

namespace DotnetTraining.Controllers
{
    [Route("api/Loops/ForEachLoop/")]
    public class ForEachController : Controller
    {
        [HttpPost, Route("Largest_Number")]         
        public IActionResult LargestNumber(LargestNumberClass largest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
			    else
			    {
                    int[]? number = largest.LargestNumber;
                    if (number!.All(_ => _ == 0)) return Ok("0");

                    List<string> newList = new();
                    foreach (int n in number!)
                    {
                        newList.Add(n.ToString());
                    }

                    newList.Sort((a, b) => (b + a).CompareTo(a + b));

                    return Ok(string.Concat(newList));

                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("removeDuplicates")]
        public IActionResult RemoveDuplicates(RemoveDuplicatesClass removeDuplicates)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? value = removeDuplicates.Value;
                    string result = string.Empty;
                    foreach (char c in value!)
                    {
                        if (!result.Contains(c))
                        {
                            result += c;
                        }
                    }
                    return Ok(result);
                }


            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("findPeakElement")]
        public IActionResult FindPeakElement(FindPeakElementClass findPeakElement)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int Peak = int.MinValue;
                    int[]? number = findPeakElement.Number;
                    //checking for peak value till the array 

                    foreach (var item in number!)
                    {
                        if (item > Peak)
                        {
                            Peak = item;
                        }
                    }
                    //after finding the peak element in an array in this loop checking for index value
                    foreach (var item in number!)
                    {
                        if (Peak == item)
                        {
                            return Ok(Array.IndexOf(number, item));
                        }
                    }

                    return Ok(-1);
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("coinChange")]
        public IActionResult CoinChange(CoinChangeClass coinChange)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? coins = coinChange.CoinNumber;
                    int amount = coinChange.Amount;
                    var dp = new long[amount + 1];
                    foreach (var i in Enumerable.Range(0, amount + 1))
                    {
                        dp[i] = int.MaxValue;
                    }
                    dp[0] = 0;

                    foreach (var i in Enumerable.Range(1, amount))
                    {
                        foreach (var coin in coins!)
                        {
                            if (coin <= i)
                            {
                                dp[i] = Math.Min(dp[i], dp[i - coin] + 1);
                            }
                        }
                    }

                    return Ok(dp[amount] > amount ? -1 : (int)dp[amount]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("findTheDifference")]
        public IActionResult FindTheDifference(FindTheDifferenceClass difference)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? value1 = difference.Value1;
                    string? value2 = difference.Value2;
                    char result = (char)0;
                    foreach (var ch in value1!)
                        result ^= ch;
                    foreach (var ch in value2!)
                        result ^= ch;

                    return Ok(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
