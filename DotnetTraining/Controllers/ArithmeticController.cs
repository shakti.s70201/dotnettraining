﻿using Microsoft.AspNetCore.Mvc;
using DotnetTraining.Models.Operator.Arithmetic;
using System.Numerics;

namespace DotnetTraining.Controllers
{
    [Route("api/operator/arithmetic/")]
    public class ArithmeticController : Controller
    {


        [HttpPost, Route("sumOfClosestTarget")]
        public ActionResult<string> SumOfClosestTarget(SumOfClosestTargetClass arithmatic1)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    //Input: nums = [-1, 2, 1, -4], target = 1
                    //Output: 2
                    //Explanation: The Sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
                    //Two Input from user.

                    int[]? numbers = arithmatic1.Numbers;
                    //int[] numbers = { -1, 2, 1, -4, 55, 8, 10 };
                    int target = arithmatic1.Target;

                    for (int i = 0; i < numbers?.Length - 1; i++)
                    {
                        for (int j = 0; j < numbers?.Length - 1; j++)
                        {
                            if (numbers?[j] > numbers?[j + 1])
                            {
                                (numbers[j], numbers[j + 1]) = (numbers[j + 1], numbers[j]);
                            }
                        }
                    }
                    List<int> max = new();
                    List<int> min = new();
                    int equal = 0;
                    foreach (int i in numbers ?? Array.Empty<int>())
                    {
                        if (target > i) { min.Add(i); }
                        if (target < i) { max.Add(i); }
                        if (target == i) { equal = i; }
                    }
                    int sum = min[^1] + equal + max[0];
                    if (equal == 0)
                    {
                        sum = (min[^1] + max[0]);
                        return ($"{min[^1]} + {max[0]} = " + sum);
                    }
                    else { return ($"{min[^1]} + {equal} + {max[0]} = " + sum); }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("add_Sub_Multi_Divi")]
        public ActionResult<string> AddSubMultiDivi(AddSubMultiDiviClass arithmatic2)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? values = arithmatic2.Values;
                    int target = arithmatic2.Target;
                    string? sign = arithmatic2.OperatorSign;
                    string showResult = "";
                    switch (sign)
                    {
                        case "+": // For 2 SUM
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target)
                                { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] > target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                {
                                    if (values[i] + values[j] == target)
                                    { showResult += $"position ({i}, {j}). values {values[i]} + {values[j]} = {target}\n"; }
                                }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        case "-": // For 2 SUB
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target)
                                { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] < target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                {
                                    if (values[i] > values[j])
                                    {
                                        if (values[i] - values[j] == target)
                                        { showResult += $"position ({i}, {j}). values {values[i]} - {values[j]} = {target}\n"; }
                                    }
                                    else
                                    {
                                        if (values[j] - values[i] == target)
                                        { showResult += $"position ({j}, {i}). values {values[j]} - {values[i]} = {target}\n"; }
                                    }
                                }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        case "*": // For 2 Multiply
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target)
                                { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] > target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                {
                                    if (values[i] * values[j] == target)
                                    { showResult += $"position ({i}, {j}). values {values[i]} * {values[j]} = {target}\n"; }
                                }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        case "/": // For 2 Division
                            for (int i = 0; i < values?.Length; i++)
                            {
                                if (values[i] == target)
                                { showResult += $"position ({i}) values {values[i]} = {target}\n"; }
                                if (values[i] < target) { continue; }
                                for (int j = i + 1; j < values.Length; j++)
                                {
                                    if (values[i] > values[j])
                                    {
                                        if (values[i] / values[j] == target)
                                        { showResult += $"position ({i}, {j}). values {values[i]} / {values[j]} = {target}\n"; }
                                    }
                                    else
                                    {
                                        if (values[j] / values[i] == target)
                                        { showResult += $"position ({j}, {i}). values {values[j]} / {values[i]} = {target}\n"; }
                                    }
                                }
                            }
                            if (showResult.Length == 0) { goto Label; }
                            return Ok(showResult);
                        default: return Ok("Please use this +, -, x, % Operator Only");
                    }
                Label: return Ok("No Matching Values Find");
                }


            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("factorialTrailingZeroes")]
        public IActionResult FactorialTrailingZeroes(FactorialClass factorial)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int divident = factorial.Number;
                    int divisor = 5;
                    int counter = 0;
                    while (divident / divisor != 0)
                    {
                        counter += divident / divisor;
                        divisor *= 5;
                    }
                    return Ok(counter);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }




        [HttpPost, Route("multiplyStrings")]
        public IActionResult MultiplyStrings(MultiplyStringsClass multyString)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? number1 = multyString.Number1;
                    string? number2 = multyString.Number2;
                    if (number1 == "0" || number2 == "0")
                        return Ok("0");
                    if (number1 == "1")
                        return Ok(number2);
                    if (number2 == "1")
                        return Ok(number1);

                    if (number1!.Length == 0 && number2!.Length == 0)
                        return Ok("");
                    if (number1.Length == 0)
                        return Ok(new string('0', number2!.Length - 1));
                    if (number2!.Length == 0)
                        return Ok(new string('0', number1.Length - 1));

                    if (number2!.Length > number1.Length)
                        (number1, number2) = (number2, number1);
                    int len1 = number1.Length;
                    int len2 = number2!.Length;

                    char[] chars = new char[len1 + len2];

                    for (int j = len2 - 1; j >= 0; j--)
                    {
                        int d2 = number2[j] - '0';
                        int complement = 0;
                        for (int i = len1 - 1; i >= 0; i--)
                        {
                            int d1 = number1[i] - '0';
                            int shift = j + i + 1;
                            int d1cur = (chars[shift] != '\0') ? chars[shift] - '0' : 0;
                            int result = d1cur + (d2 * d1) + complement;
                            chars[shift] = (char)((result % 10) + '0');
                            complement = result / 10;
                        }
                        chars[j] = (char)(complement + '0');
                    }
                    return Ok(new string(chars).TrimStart('0'));

                    //OneLine Solution
                    //return Ok(Convert.ToString(BigInteger.Parse(number1) * BigInteger.Parse(number2)));
                }


            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("calculateII")]
        public int CalculateII()
        {
            string inputs = "";
            char sign = '+';
            var nums = new Stack<int>();
            int cur = 0;

            for (int i = 0; i <= inputs.Length; i++)
            {
                if (i < inputs.Length && inputs[i] >= '0' && inputs[i] <= '9')
                    cur = cur * 10 + inputs[i] - '0';
                else if (i == inputs.Length || inputs[i] != ' ')
                {
                    if (sign == '+') nums.Push(cur);
                    else if (sign == '-') nums.Push(-cur);
                    else if (sign == '*')
                    {
                        var pre = nums.Pop();
                        nums.Push(pre * cur);
                    }
                    else if (sign == '/')
                    {
                        var pre = nums.Pop();
                        nums.Push(pre / cur);
                    }

                    if (i < inputs.Length)
                    {
                        cur = 0;
                        sign = inputs[i];
                    }
                }
            }

            var res = 0;
            while (nums.Count > 0)
                res += nums.Pop();
            return res;
        }
    }
}
