﻿using DotnetTraining.Models.Array;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Drawing;
using static System.Net.Mime.MediaTypeNames;

namespace DotnetTraining.Controllers
{
    [Route("api/multi-DimensionalArray")]
    public class Multi_DimensionalArrayController : Controller
    {

        [HttpPost, Route("diagonalTraverse")]
        public IActionResult FindDiagonalOrder([FromBody] DiagonalTraverseClass diagonal)
        {
            int[][]? matrix = diagonal.Matrix;
            if (matrix == null || matrix.Length == 0) return Ok(Array.Empty<int>());

            int N = matrix.Length;
            int M = matrix[0].Length;

            int[] result = new int[N * M];
            int row = 0, col = 0;
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = matrix[row][col];
                if ((row + col) % 2 == 0)
                {
                    if (col == M - 1) { row++; }
                    else if (row == 0) { col++; }
                    else { row--; col++; }
                }
                else
                {
                    if (row == N - 1) { col++; }
                    else if (col == 0) { row++; }
                    else { row++; col--; }
                }
            }

            return Ok(result);
        }



        [HttpPost, Route("matrixDiagonalSum")]
        public IActionResult MatrixDiagonalSum([FromBody] MatrixDiagonalSumClass DiagonalSum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[][]? matrix = DiagonalSum.MatrixSum;
                    int sum = 0;
                    for (int i = 0; i < matrix!.Length; i++)
                    {
                        for (int j = 0; j < matrix.Length; j++)
                        {
                            if (i == j)
                            {
                                sum += matrix[i][j];
                                continue;
                            }
                            if (i + j == matrix.Length - 1)
                            {
                                sum += matrix[i][j];
                            }
                        }
                    }
                    return Ok(sum);
                }


            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost,Route("islandPerimeter")]
        public IActionResult IslandPerimeter([FromBody] IslandPerimeterClass island)
        {
            int[][]? grid = island.IslandGrid;
            int result = 0;
            int row = grid!.Length;
            int col = grid[0].Length;
            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                {
                    if (grid[i][j] == 1)
                    {
                        result += 4;
                        if (i > 0 && grid[i - 1][j] == 1)
                            result -= 2;
                        if (j > 0 && grid[i][j - 1] == 1)
                            result -= 2;
                    }
                }

            return Ok(result);
        }




        [HttpPost, Route("sortMatrixDiagonally")]
        public IActionResult SortMatrixDiagonally([FromBody] SortMatrixDiagonallyClass sortDiaonally)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[][]? matrix = sortDiaonally.MatrixSort;
                    int n = matrix!.Length;
                    int m = matrix[0].Length;

                    Dictionary<int, SortedDictionary<int, int>> map = new();
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            if (!map.ContainsKey(i - j))
                            {
                                map[i - j] = new SortedDictionary<int, int>();
                            }
                                
                            if (map[i - j].ContainsKey(matrix[i][j]))
                            {
                                map[i - j][matrix[i][j]]++;
                            }
                            else
                            {
                                map[i - j][matrix[i][j]] = 1;
                            }
                        }
                    }

                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < m; j++)
                        {
                            int value = map[i - j].Keys.First();
                            map[i - j][value]--;
                            if (map[i - j][value] == 0)
                            {
                                map[i - j].Remove(value);
                            }

                            matrix[i][j] = value;
                        }
                    }

                    return Ok(matrix);
                }


            }
            catch (Exception)
            {
                throw;
            }
        }



        [HttpPost, Route("floodFill")]
        public IActionResult FloodFill([FromBody] FloodFillClass floodFill)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[][]? image = floodFill.ImageArray;
                    int sr = floodFill.Sr;
                    int sc = floodFill.Sc;
                    int newColor = floodFill.Color;
                    int oldColor = image![sr][sc];
                    if (oldColor == newColor)
                    {
                        return Ok(image);
                    }

                    var rows = image.Length;
                    var cols = image[0].Length;
                    var queue = new Queue<(int row, int col)>();
                    queue.Enqueue((sr, sc));
                    while (queue.Count > 0)
                    {
                        (int row, int col) = queue.Dequeue();
                        image[row][col] = newColor;

                        if (row > 0 && image[row - 1][col] == oldColor) queue.Enqueue((row - 1, col));
                        if (row < rows - 1 && image[row + 1][col] == oldColor) queue.Enqueue((row + 1, col));
                        if (col > 0 && image[row][col - 1] == oldColor) queue.Enqueue((row, col - 1));
                        if (col < cols - 1 && image[row][col + 1] == oldColor) queue.Enqueue((row, col + 1));
                    }

                    return Ok(image);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
