﻿using DotnetTraining.Models.ForLoops;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace DotnetTraining.Controllers
{
    [Route("api/Loops/ForLoop")]
    public class ForLoopController : Controller
    {

        [HttpPost, Route("Kth_Maximum_&_Minimum")]
        public IActionResult ForLoopsFirst(ForLoop1 forLoop1)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? arrayValues = forLoop1.Values ?? Array.Empty<int>();

                    int target = forLoop1.Target;

                    for (int i = 0; i < arrayValues?.Length - 2; i++)
                    {
                        for (int j = 0; j < arrayValues?.Length - 1; j++)
                        {

                            if (arrayValues?[j] > arrayValues?[j + 1])
                            {
                                (arrayValues[j], arrayValues[j + 1]) = (arrayValues[j + 1], arrayValues[j]);
                            }
                        }
                    }

                    int value1 = Convert.ToInt32(arrayValues?[target - 1]);

                    int value2 = Convert.ToInt32(arrayValues?[^target]);

                    return Ok($"Kth Minimum Value is {value1},\n"
                    + $"Kth Maximum Value is {value2},\n");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("disappeared_Numbers")]                        //Input value must be in range
        public IActionResult DisappearedNumbers(DisappearedNumbersClass disappearedNumbers)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int[]? arrayNumber = disappearedNumbers.NumberArray;
                    var set = new HashSet<int>();
                    for (int i = 1; i <= arrayNumber!.Length; i++)
                    {
                        set.Add(i);
                    }

                    for (int i = 0; i < arrayNumber.Length; i++) set.Remove(arrayNumber[i]);
                    {
                        return Ok(set.ToList());
                    }

                    ////One Line Code
                    //return  Ok(Enumerable.Range(1, arrayNumber!.Length).ToHashSet().Except(arrayNumber).ToList());

                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("countPrimeNumber")]
        public IActionResult CountPrimeNumber(CountPrimeNumberClass countPrimeNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    int number = countPrimeNumber.Number;
                    if (number <= 1)
                        return Ok(0);

                    bool[] arr = new bool[number];

                    for (int i = 2; i <= (int)Math.Sqrt(number); i++)
                    {
                        if (arr[i] == false)
                        {
                            for (int j = i * i; j < number; j += i)
                            {
                                arr[j] = true;
                            }
                        }
                    }

                    int count = 0;
                    for (int i = 2; i < number; i++)
                    {
                        if (!arr[i])
                            count++;
                    }

                    return Ok(count);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }




        [HttpPost, Route("containsDuplicateII")]
        public IActionResult ContainsNearbyDuplicate(ContainsDuplicateIIClass duplicateII)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {

                    int[]? number = duplicateII.DuplicteNumber;
                    int target = duplicateII.Target;

                    bool result = false;
                    for (int i = 0; i < number!.Length; i++)
                    {
                        int a = Math.Min((i + target), number.Length - 1);
                        for (int j = i; j <= a; j++)
                        {
                            if (number[i] == number[j] && i != j)
                            {
                                result = true;
                                return Ok(result);
                            }
                        }
                    }
                    return Ok(result);

                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }



        [HttpPost, Route("licenseKeyFormatting")]
        public IActionResult LicenseKeyFormatting(LicenseKeyFormattingClass licenseKey)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                    });
                }
                else
                {
                    string? formatString = licenseKey.FormatString;
                    int target = licenseKey.Target;
                    // Remove all hyphens and convert to upper case
                    string str = formatString!.Replace("-", "").ToUpper();

                    // Find the length of the first group
                    int firstGroupLen = str.Length % target;

                    StringBuilder stringBuilder = new();

                    // Append the first group to the result string using a for loop
                    for (int i = 0; i < firstGroupLen; i++)
                    {
                        stringBuilder.Append(str[i]);
                    }

                    // Append the remaining groups to the result string using a for loop
                    for (int i = firstGroupLen; i < str.Length; i += target)
                    {
                        if (stringBuilder.Length > 0)
                        {
                            stringBuilder.Append('-');
                        }
                        for (int j = 0; j < target; j++)
                        {
                            if (i + j < str.Length)
                            {
                                stringBuilder.Append(str[i + j]);
                            }
                        }
                    }

                    return Ok(stringBuilder.ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        //[HttpPost, Route("combination")]
        //public IActionResult Conbination(combinationClass combination)
        //{
        //    int number = combination.Number;
        //    int target = combination.Target;
        //    List<IList<int>> list = new();
        //    if (target == 1)
        //    {
        //        for (int i = 1; i <= number; i++) list.Add(new List<int> { i });
        //        return Ok(list);
        //    }
        //    var lists = Combine(number, target - 1);
        //    for (int i = 0; i < lists.Count; i++)
        //    {
        //        var len = lists[i][lists[i].Count - 1];
        //        for (int j = len + 1; j <= number; j++)
        //        {
        //            list.Add(Combine(j, lists[i]));
        //        }
        //    }
        //    return Ok(list);
        //}
        //private List<int> Combine(int num, IList<int> lists)
        //{
        //    var list = new List<int>(lists);
        //    list.Add(num);
        //    return list;
        //}
    }
}
